#!/usr/bin/python3

from collections import deque
import matplotlib.pyplot as plt

currency_lifetime = 100 # how long a unit of currency exist in years
#currency_periods  = currency_lifetime * 12 # how many periods exist
currency_periods = 999
#currency_increase = 1000.0 # how many units of currency to create per period
currency_increase = 2000.0 # how many units of currency to create per period

class Ledger:
  class Account:
    def __init__(self, ledger, balance, natural=False):
      self.ledger = ledger
      self.recent = ledger.period
      self.balance = balance
      self.natural = natural
    def adjust(self, amount):
      if not self.exists():
        return
      self.balance += amount
      self.recent = self.ledger.period
    def exists(self):
      return id(self) in self.ledger.accounts
    def expire(self):
      if not self.exists():
        return
      self.ledger.reference.balance += self.balance
      self.balance = 0.0
      del self.ledger.accounts[id(self)]

  def __init__(self, previous):
    self.period = 0
    self.periods = deque()
    for i in range(currency_periods):
      self.periods.append(0.0)
    self.periods[len(self.periods) - 1] = float(previous)
    self.reference = Ledger.Account(self, 0.0, natural=False)
    self.accounts = {}
    self.accounts[id(self.reference)] = self.reference

  def account(self, i):
    if not i in self.accounts:
      return None
    return self.accounts[i]

  def create(self, balance, natural=False):
    a = Ledger.Account(self, balance, natural)
    self.accounts[id(a)] = a
    return a

  def tick(self):
    # update period
    self.period += 1

    # calculate period increase absolute amount
    # calculate account base after applying increase and before applying decrease
    # apply account balance increase
    period_increase = 0.0
    account_pre_base = 0.0
    for account in self.accounts.values():
      account_pre_base += account.balance
      if account.natural:
        period_increase += currency_increase
        account_pre_base += currency_increase
        account.balance += currency_increase

    # calculate period decrease absolute amount
    # calculate period base
    period_decrease = 0.0
    period_base = period_increase
    for i in self.periods:
      period_decrease += i / float(currency_periods)
      period_base += i

    # retire previous period
    self.periods.popleft()
    # apply period increase
    self.periods.append(period_increase)

    print('record', self.period, 'base', period_base, 'increase', period_increase, 'decrease', period_decrease)

    # calculate account adjustment ratio based on the period decrease absolute amount divided by account base
    ratio = period_decrease / account_pre_base

    # apply account decrease by adjustment ratio
    # calculate total account decrease by ratio
    account_decrease = 0.0
    account_post_base = 0.0
    for account in self.accounts.values():
      account_decrease += account.balance * ratio
      account.balance -= account.balance * ratio
      if account.balance < 0.0:
        account.balance = 0.0
      account_post_base += account.balance

    # apply absolute period decrease vs total account decrease by ratio to reference account to correct floating point error
    self.reference.adjust(period_decrease - account_decrease)

    print('ledger', self.period, 'base', account_post_base, 'increase', period_increase, 'decrease', account_decrease)

def run():
  ledger = Ledger(0.0)
  account = ledger.create(0.0, natural=True)
  history = []

  for i in range(currency_periods * 2):
    ledger.tick()
    history.append(account.balance)

  account.natural = False

  while account.balance > 0.01:
    ledger.tick()
    history.append(account.balance)

  plt.plot(history)
  plt.show()

run()

